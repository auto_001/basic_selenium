package com.mercury.test;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

public class MR {
	
	// A new line has been added
	
	// A new line further added ---- 1234 ---- 45678

	static WebDriver driver;
	//public static final Logger log = Logger.getLogger(MR.class);

	// ===============================================================================
	
	/*	
	 *	Browser launch and application launch 
	 */
	public void browserMercuryAppLaunch() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();
		
		//log.info("Browser has been launched");
		
		
		Thread.sleep(1000);
		driver.manage().window().maximize();
		
		
		Thread.sleep(1000);
		driver.get("http://demo.guru99.com/test/newtours/");
		Thread.sleep(1000);
		//driver.close();
	}
	
	
	public void loginFunctionality() {
		
		WebElement loginName = driver.findElement(By.name("userName"));
		loginName.sendKeys("dasd");
		
		WebElement loginPassword = driver.findElement(By.name("password"));
		loginPassword.sendKeys("dasd");
		
		
		
		WebElement loginSubmit = driver.findElement(By.xpath("//input[@name='submit']"));
		//WebElement loginSubmit = driver.findElement(By.name("submit"));
		loginSubmit.click();
		
	}
	
	
	
	
	
	
	// ============================================================================
	
	/* Closing browser */
	public void browserClose()
	{
		driver.close();
	}
	
	// =============================================================================
	
	/* Closing browser with static method */
	public static void browserCloseStatic()
	{
		driver.close();
	}

	// ===============================================================================

	/*
	 * Simple login process with xpath locator
	 */
	public void loginMercury() throws IOException, InterruptedException {
		WebElement uName = driver.findElement(By.xpath("//input[@name='userName']"));
		
		//WebElement uName = driver.findElement(By.name("userName"));
		uName.sendKeys("dasd");
		Thread.sleep(3000);
		
		WebElement pwd = driver.findElement(By.name("password"));
		pwd.sendKeys("dasd");
		Thread.sleep(3000);
		
		WebElement login = driver.findElement(By.name("submit"));
		login.click();
	}
	
	// =====================================================================	
	
	/*
	 * Simple login process with parameterized login method.
	 */
	public void loginMercuryWithParameter(String username, String password) throws IOException, InterruptedException {
		
		WebElement uName = driver.findElement(By.xpath("//input[@name='userName']"));
		uName.sendKeys(username);
		WebElement pwd = driver.findElement(By.name("password"));	
		pwd.sendKeys(password);
		WebElement login = driver.findElement(By.name("submit"));
		login.click();
		Thread.sleep(3000);
	}
	
	
	public void departFromSelect() throws InterruptedException {
		WebElement flightLink = driver.findElement(By.linkText("Flights"));
		flightLink.click();
		
		Thread.sleep(5000);
		
		WebElement deptDropDown = driver.findElement(By.name("fromPort"));
		Select sl = new Select(deptDropDown);
		//sl.selectByVisibleText("London");
		//sl.selectByValue("Paris");
		sl.selectByIndex(8);
		
	}

	// ============================================================ //
	
	/*
	 * Verify Login
	 */
	public void verifyLogin()
	{
		String expTitle = "Login: Mercury Tours";
		String actTitle = driver.getTitle();
		
		System.out.println(" EXPECTING BY US System returns the page title: "+ expTitle);
		
		System.out.println(" PRINTED BY SELENIUM System returns the page title: "+ actTitle);
		
		if(actTitle.equals(expTitle))
		{
			System.out.println("Test case passed");
		}
		else
		{
			System.out.println("Test case failed");
		}
		
	}
	
	// ===========================================================
	
	/* 
	 * Verify Login with boolean return type
	 */
	public boolean verifyLoginWithReturn()
	{
		String actTitle = "Find a Flight: Mercury Tours:";
		String expTitle = driver.getTitle();
		
		System.out.println("System returns the page title: "+ expTitle);
		
		if(actTitle.equals(expTitle))
		{
			System.out.println("Test case passed");
			return true;
		}
		else
		{
			System.out.println("Test case failed");
			return false;
		}
		
	}

	
	// ==============================================================================
	
		public void selectCity() throws InterruptedException
		{
			WebElement departFrom = driver.findElement(By.name("fromPort"));
			Select sel = new Select(departFrom);
			Thread.sleep(4000);
			sel.selectByVisibleText("Paris");
			sel.selectByIndex(6);
			sel.selectByValue("New York");
			
			
		}
	

	// ===============================================================================

	/*
	 * Screenshot Mechanism
	 */
	public void screenShot() throws IOException, InterruptedException {

		File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(src, new File("./screenshots/snap.png"));

		Thread.sleep(3000);
	}

	// ================================================================================

	/*
	 * Login with Sikuli [Image identification will take place instead of Object]
	 */
	public void loginSikuli() throws FindFailed {
		
		WebElement uName = driver.findElement(By.xpath("//input[@name='userName']"));
		uName.sendKeys("dasd");
		Screen screen = new Screen();
		Pattern password = new Pattern("./sikulifiles/password.PNG");
		screen.click(password);
		screen.type(password, "dasd");
		Pattern btnLogin = new Pattern("./sikulifiles/btnlogin.PNG");
		screen.click(btnLogin);
		screen.type("Login is successful with Sikuli utility");
		
	}

	// ================================================================================

	/*
	 * Login with Robot
	 */
	public void loginRobot() throws AWTException, InterruptedException {
		WebElement uName = driver.findElement(By.xpath("//input[@name='userName']"));
		uName.sendKeys("dasd");
		WebElement pwd = driver.findElement(By.xpath("//input[@name='password']"));
		pwd.sendKeys("dasd");
		Thread.sleep(2000);
		
		Robot r1 = new Robot();
		r1.keyPress(KeyEvent.VK_TAB);
		Thread.sleep(3000);
		r1.keyPress(KeyEvent.VK_ENTER);
	}

	// =================================================================================

	/*
	 * Login with Action
	 */
	public void loginAction() {
		WebElement uName = driver.findElement(By.xpath("//input[@name='userName']"));
		uName.sendKeys("dasd");

		WebElement pwd = driver.findElement(By.xpath("//input[@name='password']"));
		pwd.sendKeys("dasd");

		WebElement login = driver.findElement(By.xpath("//input[@value='Submit']"));

		Actions builder = new Actions(driver);

		builder.moveToElement(login).build().perform();

		builder.click(login).build().perform();
	}

	// ================================================================================

	/*
	 * Photo Upload with AutoIT
	 */
	public void autoITTest() throws InterruptedException, IOException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://imgbb.com/");
		Thread.sleep(4000);
		WebElement btnStartUpload = driver.findElement(By.xpath("//a[@class='btn btn-big blue']"));
		btnStartUpload.click();
		Thread.sleep(4000);
		
		
		Runtime.getRuntime().exec("./autoitexe/autoitfileupload.exe");
		
		Thread.sleep(3000);
		
		
		Thread.sleep(6000);
		WebElement btnUpload = driver.findElement(By.xpath("//button[@class='btn btn-big green']"));
		btnUpload.click();
		Thread.sleep(5000);
	}

	// ================================================================================

	/*
	 * Login with ImplicitWait
	 */
	public void loginImplicitWait() {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		WebElement uName = driver.findElement(By.xpath("//input[@name='userName']"));
		uName.sendKeys("dasd");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		WebElement pwd = driver.findElement(By.xpath("//input[@name='password']"));
		pwd.sendKeys("dasd");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		WebElement login = driver.findElement(By.xpath("//input[@value='Login']"));
		login.click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	// ================================================================================

	/*
	 * Login with Explicit Wait
	 */
	public void loginExplicite() {
		new WebDriverWait(driver, 10)
				.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='userName']")));
		
		WebElement uName = driver.findElement(By.xpath("//input[@name='userName']"));
		uName.sendKeys("dasd");
		
		new WebDriverWait(driver, 10)
				.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='password']")));

		WebElement pwd = driver.findElement(By.xpath("//input[@name='password']"));
		pwd.sendKeys("dasd");

		new WebDriverWait(driver, 10)
				.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@value='Login']")));

		WebElement login = driver.findElement(By.xpath("//input[@value='Login']"));
		login.click();
	}

	// ==================================================================================

	/*
	 * Login with Property File Data
	 */
	public void loginWithPropertyFile() throws IOException, InterruptedException {
		
		
		File file = new File("./testdata/testdata.properties");
		FileInputStream fileInput = new FileInputStream(file);
		
		Properties prop = new Properties();
		
		prop.load(fileInput);
		Thread.sleep(3000);
		
		
		WebElement uName = driver.findElement(By.xpath("//input[@name='userName']"));
		uName.sendKeys(prop.getProperty("admin_user"));
		Thread.sleep(2000);
		WebElement pwd = driver.findElement(By.xpath("//input[@name='password']"));
		pwd.sendKeys(prop.getProperty("admin_Password"));
		Thread.sleep(2000);
		WebElement login = driver.findElement(By.xpath("//input[@value='Submit']"));
		login.click();

	}

	// =================================================================================

	/*
	 * Data Driven Login testing with Excel
	 */
	public void loginDataDrivenExcelSheet() throws BiffException, IOException, InterruptedException {
		File src = new File("./testdata/exceldata.xls");
		Workbook wb = Workbook.getWorkbook(src);
		Sheet sh1 = wb.getSheet("login");

		int rows = sh1.getRows();

		for (int i = 1; i < rows; i++) {
			String userNameDataFromExcel = sh1.getCell(0, i).getContents();
			String passwordDataFromExcel = sh1.getCell(1, i).getContents();

			Thread.sleep(2000);

			WebElement uName = driver.findElement(By.xpath("//input[@name='userName']"));
			uName.sendKeys(userNameDataFromExcel);
			Thread.sleep(2000);

			WebElement pwd = driver.findElement(By.xpath("//input[@name='password']"));
			pwd.sendKeys(passwordDataFromExcel);
			Thread.sleep(2000);

			WebElement login = driver.findElement(By.xpath("//input[@value='Submit']"));
			login.click();
			Thread.sleep(5000);
			

			WebElement home = driver.findElement(By.xpath("//a[contains(text(),'Home')]"));
			home.click();
			Thread.sleep(5000);

		}
	}

	// =====================================================================================

	/*
	 * Scroll down handling
	 */
	public void scrollDown() throws InterruptedException
	{
			System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
            driver = new ChromeDriver();
            driver.manage().window().fullscreen();
            driver.get("http://toolsqa.com/iframe-practice-page/");
            Thread.sleep(6000);         
            JavascriptExecutor js = (JavascriptExecutor) driver;
            js.executeScript("window.scrollBy(0, 6000)");
            Thread.sleep(9000);                         
	}
	
	//======================================================================================

	/*
	 * Iframe Handling
	 */
	public void frameHandling() throws InterruptedException {
		
			System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		 	driver = new ChromeDriver();
		 	driver.manage().window().maximize();
        	driver.get("http://demo.guru99.com/test/guru99home/");
        	Thread.sleep(6000); 
        	
        	
        	JavascriptExecutor js = (JavascriptExecutor) driver;
        	js.executeScript("window.scrollBy(0,2300)");
        	Thread.sleep(6000);  
        	
        	
        	driver.switchTo().frame("a077aa5e");
        	Thread.sleep(4000); 
        	WebElement linkbtn = driver.findElement(By.xpath("//img[@src='Jmeter720.png']"));
        	linkbtn.click();
        	driver.switchTo().defaultContent();
    
        	/* Iframe Identification */
        	
        	int size = driver.findElements(By.tagName("iframe")).size();
        	System.out.println(size);
        	List<WebElement> ele = driver.findElements(By.tagName("iframe"));
            System.out.println("Number of frames in a page :" + ele.size());
            for(WebElement el : ele){
                System.out.println("Frame Id :" + el.getAttribute("id"));
                System.out.println("Frame name :" + el.getAttribute("name"));
            }
	}
	
	//=======================================================================================

	
	/*
	 * Web Table handling mechanism
	 */
	public void webTableHandling() {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("http://demo.guru99.com/test/web-table-element.php"); 
		driver.manage().window().maximize();
		
	    //No.of Columns
	    List<WebElement>  col = driver.findElements(By.xpath(".//*[@id='leftcontainer']/table/thead/tr/th"));
	    System.out.println("No of cols are : " +col.size()); 
	    
	    //No.of rows 
	    List<WebElement>  rows = driver.findElements(By.xpath(".//*[@id='leftcontainer']/table/tbody/tr/td[1]")); 
	    System.out.println("No of rows are : " + rows.size());
	    //driver.close();
	    
	    // Dynamic data searching
	    for(int i=1; i<rows.size(); i++)
	    {
	    	if(driver.findElement(By.xpath(".//*[@id='leftcontainer']/table/tbody/tr[" + i + "]/td[1]")).getText().equals("YES Bank Ltd."))
	    	{
	    		if(driver.findElement(By.xpath(".//*[@id='leftcontainer']/table/tbody/tr[" + i + "]/td[1]")).getText()!=null)
	    		{
	    		System.out.println(driver.findElement(By.xpath(".//*[@id='leftcontainer']/table/tbody/tr[" + i + "]/td[4]")).getText());
	    		}
	    		else
	    		{
	    			System.out.println("Required data not found");
	    		}
	    		break;
	    	}
	    }
	    

	}
	
	//========================================================================================
	
	/* Pop handling */
	
	public void popUpHandling() throws InterruptedException
	{
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		WebDriver driver = new ChromeDriver();
	
        
        // Alert Message handling
                    		
        driver.get("http://demo.guru99.com/test/delete_customer.php");	
        driver.manage().window().maximize();
                            		
     	      	
        driver.findElement(By.name("cusid")).sendKeys("sasassa");	
        Thread.sleep(3000);
        driver.findElement(By.name("submit")).submit();		
        Thread.sleep(5000);
        		
        // Switching to Alert        
        Alert alert = driver.switchTo().alert();	
        driver.switchTo().alert().dismiss();
        		
        // Capturing alert message.    
        //String alertMessage= driver.switchTo().alert().getText();		
        		
        // Displaying alert message		
        //System.out.println(alertMessage);	
        //Thread.sleep(5000);
        		
        // Accepting alert		
        
    }	
	
	public void test() {
		
		String name = "Arijita";
		int Roll_no = 25;
		
		System.out.println("Name of the student is "+ name + " and Roll No: " + Roll_no);
	}

	}
	
	//=========================================================================================


