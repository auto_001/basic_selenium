package com.mercury.test;

import java.awt.AWTException;
import java.io.IOException;

import org.sikuli.script.FindFailed;

import jxl.read.biff.BiffException;

public class Driver {

	public static void main(String[] args) throws FindFailed, AWTException, InterruptedException, IOException, BiffException {
		MR m1 = new MR();
		//m1.browserMercuryAppLaunch();
		//m1.loginDataDrivenExcelSheet();
		m1.frameHandling();
		
	}

}
